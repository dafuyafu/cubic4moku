#include <stdio.h>
#include <stdlib.h>
#include "bd.h"

int board[4][4][4]; 
int stack[4][4][4];
int move;

void initialize(){
	move = 0;
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			for(int k = 0; k < 4; k++){
				board[i][j][k] = 0;
			}
		}
	}
}

void printBoard(){
	int flag;
	move ++;
	for(int i = 0; i < 4; i++){
		flag = 0;
		printf("Level %d\n",i);
		for(int j = 0; j < 4; j++){
			for(int k = 0; k < 4; k++){
				printf("%d",board[k][j][i]);
				if(top(k, j) > i + 1){
					flag = 1;
				}
			}
			printf("\n");
		}
		printf("\n");
		if(!flag) break;
	}
}

void printTurn(int t){
	if (t == 0){
		printf("It's white's turn.\n");
	}else if(t == 1){
		printf("It's black's turn.\n");
	}else{
		printf("input error.\n");
		exit(1);
	}
}

void printMoves(){
	printf("moves:%d\n",move);
}

int top(int x, int y){
	int i;
	for(i = 0; i < 4; i++){
		if(board[x][y][i] == 0){
			break;
		}
	}
	return i;
}

void copyToStack(){
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			for(int k = 0; k < 4; k++){
				stack[i][j][k] = board[i][j][k];
			}
		}
	}
}

void resetFromStack(){
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			for(int k = 0; k < 4; k++){
				board[i][j][k] = stack[i][j][k];
			}
		}
	}
}

void put(int x, int y, int t){
	board[x][y][top(x, y)] = t + 1;
}

void printMovesToRecord(FILE *p){
	fprintf(p, "%d. ", move / 2 + 1);
}