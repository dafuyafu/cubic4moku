#include <stdio.h>
#include "bd.h"
#include "mv.h"

int moveForHuman(){
	int x, y, place;
	while(1){
		printf("Input the number ... > ");

		/* validate input number */
		if(scanf("%d",&place) == 0){
			scanf("%*[^\n]%*c");
			printf("input error\n");
			continue;
		}

		if(place < 11 || place > 44){
			printf("error: invalid input([%d])\n",place);
			place = 0;
			continue;
		}

		x = place / 10 - 1;
		y = place % 10 - 1;

		if( x < 0 || y < 0 || x > 3 || y > 3){
			printf("error: invalid input([%d])\n",place);
			place = 0;
			continue;
		}

		if(top(x,y) == 4){
			printf("error: [%d] is full\n", place);
			place = 0;
			continue;
		}

		break;
	}

	return place;
}