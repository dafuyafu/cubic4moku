#include <stdio.h>
#include <stdlib.h>
#include "ai_1.h"
#include "cl.h"
#include "bd.h"
#include "ov.h"

int ai_1(int t){
	copyToStack();
	int value[16];
	int x, y;
	for(int i = 0; i < 16; i++){
		x = i % 4;
		y = i / 4;

		/* If (x,y) is full, go next. */
		if(top(x,y) == 4){
			value[i] = -1;
			continue;
		}

		/* Suppose put my stone on (x, y). If the game is over, set its value to 100. */
		put(x, y, t);
		if(isOver(x, y, top(x, y) - 1, t)){
			value[i] = 100;
			continue;
		}
		resetFromStack();

		/* Suppose put the other stone on (x, y). If the game is over, set its value to 99. */
		put(x, y, !t);
		if(isOver(x, y, top(x, y) - 1, !t)){
			value[i] = 99;
			continue;
		}
		resetFromStack();

		/* Suppose put my stone on (x, y). Count the lines which are still alive and set its value to the number. */
		put(x, y, t);
		value[i] = countLines(x, y, top(x, y) - 1, t);
		if(rand() % 1000 < 6) value[i] = 10; // correctness 90%
		resetFromStack();
	}

	resetFromStack();
	int max = 0;
	for(int i = 1; i < 16; i++){
		if(value[max] < value[i]) max = i;
	}

	return (max % 4 + 1) * 10 + max / 4 + 1;
}