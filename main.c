/*

	cubic 4-moku player
	by dafuyafu 2017.

	remark: turns, white or black, is represented by 0 and 1 respectively, but on board, its stones are represented by 1 and 2.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "op.h"
#include "bd.h"
#include "ov.h"
#include "mv.h"
#include "ai_1.h"

int main(){
	int setting = opening();
	initialize();

	FILE *fp = fopen("record.txt","w");
	initialRecord(fp);
	int turn = 0;
	int place, x, y;
	printTurn(turn); // varidate the value of turn and print it
	printMovesToRecord(fp);
	srand((unsigned int)time(NULL));
	while(1){
		if(turn){
			switch(setting){
				case 1:
				case 3:
				place = moveForHuman();
				break;

				case 2:
				case 4:
				place = ai_1(turn);
				printf("Input the number ... > %d%d\n", place / 10 , place % 10);
				break;
			}
			fprintf(fp, "%d%d\n",place / 10, place % 10);
			printMovesToRecord(fp);
		}else{
			switch(setting){
				case 1:
				case 2:
				place = moveForHuman();
				break;

				case 3:
				case 4:
				place = ai_1(turn);
				printf("Input the number ... > %d%d\n", place / 10, place % 10);
				break;
			}
			fprintf(fp, "%d%d ",place / 10, place % 10);
		}

		x = place / 10 - 1;
		y = place % 10 - 1;

		put(x, y, turn);
		printBoard();
		if(isOver(x, y, top(x, y) - 1, turn)) break;
		turn = !turn;
		printTurn(turn);
	}
	fprintf(fp, "\n");
	if(turn == 0){
		printf("White win.\n");
		fprintf(fp, "White win.\n");
	}else if(turn == 1){
		printf("Black win.\n");
		fprintf(fp, "Black win.\n");
	}else{
		printf("draw.\n");
		fprintf(fp, "Draw.\n");
	}
	printMoves();
	fclose(fp);
	return 0;
}