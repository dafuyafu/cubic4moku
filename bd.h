#ifndef _BD_H_
#define _BD_H_

void initialize();
void printBoard();
void printTurn(int t);
void printMoves();
void copyToStack();
void resetFromStack();
int top(int x, int y);
void put(int x, int y, int t);
void printMovesToRecord(FILE *p);

#endif