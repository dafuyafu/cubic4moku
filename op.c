#include <stdio.h>
#include "op.h"

int opening(){
	int setting = 0;
	printf("\n");
	printf("########################################\n");
	printf("####                                ####\n");
	printf("#### cubic 4-moku by dafuyafu 2017. ####\n");
	printf("####                                ####\n");
	printf("########################################\n");
	printf("\n");

	printf("select game setting.\n");
	printf("[1] : human vs human \n");
	printf("[2] : human vs computer \n");
	printf("[3] : computer vs human \n");
	printf("[4] : computer vs computer \n");
	while(1){
		printf("Input the number ... > ");
		if(scanf("%d", &setting) == 0){
			scanf("%*[^\n]%*c");
			printf("input error\n");
			continue;
		}
		if(setting < 1 || setting > 4){
			printf("error : input a number 1, 2, 3 or 4.\n");
			continue;
		}
		break;
	}
	return setting;
}

void initialRecord(FILE *p){
	fprintf(p, "Cubic 4moku Player - dafuyafu 2017\n\n");
}