#include <stdio.h>
#include "cl.h"
#include "bd.h"

extern int board[4][4][4];


/* (x,y,z) is my color. Count the lines which are still alive, it is to say that the lines are my color or blank. */

int countLines(int x, int y, int z, int t){
	int lines = 0;
	int color = !t + 1; // this color is the other one's

	/* parallel lines */
	if(board[0][y][z] != color && board[1][y][z] != color && board[2][y][z] != color && board[3][y][z] != color){
		lines++;
	}

	if(board[x][0][z] != color && board[x][1][z] != color && board[x][2][z] != color && board[x][3][z] != color){
		lines++;
	}

	if(board[x][y][0] != color && board[x][y][1] != color && board[x][y][2] != color && board[x][y][3] != color){
		lines++;
	}

	/* diagonals on the z-plane and the cubic */
	if(x == y){
		if(y == z){
			if(board[0][0][0] != color && board[1][1][1] != color && board[2][2][2] != color && board[3][3][3] != color){
				lines ++;
			}
		}else if(y + z == 3){
			if(board[0][0][3] != color && board[1][1][2] != color && board[2][2][1] != color && board[3][3][0] != color){
				lines ++;
			}
		}
		if(board[0][0][z] != color && board[1][1][z] != color && board[2][2][z] != color && board[3][3][z] != color){
			lines ++;
		}
	}
	if(x + y == 3){
		if(y == z){
			if(board[0][3][3] != color && board[1][2][2] != color && board[2][1][1] != color && board[3][0][0] != color){
				lines ++;
			}
		}else if(y + z == 3){
			if(board[3][0][3] != color && board[2][1][2] != color && board[1][2][1] != color && board[0][3][0] != color){
				lines ++;
			}
		}
		if(board[3][0][z] != color && board[2][1][z] != color && board[1][2][z] != color && board[0][3][z] != color){
			lines ++;
		}
	}
	// diagonals on the x-plane
	if(y == z){
		if(board[x][0][0] != color && board[x][1][1] != color && board[x][2][2] != color && board[x][3][3] != color){
			lines ++;
		}
	}
	if(y + z == 3){
		if(board[x][3][0] != color && board[x][2][1] != color && board[x][1][2] != color && board[x][0][3] != color){
			lines ++;
		}
	}
	// diagonals on the y-plane
	if(x == z){
		if(board[0][y][0] != color && board[1][y][1] != color && board[2][y][2] != color && board[3][y][3] != color){
			lines ++;
		}
	}
	if(x + z == 3){
		if(board[3][y][0] != color && board[2][y][1] != color && board[1][y][2] != color && board[0][y][3] != color){
			lines ++;
		}
	}

	return lines;
}