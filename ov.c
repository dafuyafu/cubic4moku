#include "ov.h"

extern int board[4][4][4];

int isOver(int x, int y, int z, int t){
	int color = t + 1;
	// parallel lines
	if(color == board[0][y][z] && board[0][y][z] == board[1][y][z] && board[1][y][z] == board[2][y][z] && board[2][y][z] == board[3][y][z]){
		return 1;
	}

	if(color == board[x][0][z] && board[x][0][z] == board[x][1][z] && board[x][1][z] == board[x][2][z] && board[x][2][z] == board[x][3][z]){
		return 1;
	}

	if(color == board[x][y][0] && board[x][y][0] == board[x][y][1] && board[x][y][1] == board[x][y][2] && board[x][y][2] == board[x][y][3]){
		return 1;
	}

	/* diagonals on the z-plane and the cubic */
	if(x == y){
		if(y == z){
			if(color == board[0][0][0] && board[0][0][0] == board[1][1][1] && board[1][1][1] == board[2][2][2] && board[2][2][2] == board[3][3][3]){
				return 1;
			}
		}else if(y + z == 3){
			if(color == board[0][0][3] && board[0][0][3] == board[1][1][2] && board[1][1][2] == board[2][2][1] && board[2][2][1] == board[3][3][0]){
				return 1;
			}
		}
		if(color == board[0][0][z] && board[0][0][z] == board[1][1][z] && board[1][1][z] == board[2][2][z] && board[2][2][z] == board[3][3][z]){
			return 1;
		}
	}
	if(x + y == 3){
		if(y == z){
			if(color == board[0][3][3] && board[0][3][3] == board[1][2][2] && board[1][2][2] == board[2][1][1] && board[2][1][1] == board[3][0][0]){
				return 1;
			}
		}else if(y + z == 3){
			if(color == board[3][0][3] && board[3][0][3] == board[2][1][2] && board[2][1][2] == board[1][2][1] && board[1][2][1] == board[0][3][0]){
				return 1;
			}
		}
		if(color == board[3][0][z] && board[3][0][z] == board[2][1][z] && board[2][1][z] == board[1][2][z] && board[1][2][z] == board[0][3][z]){
			return 1;
		}
	}

	/* diagonals on the x-plane */
	if(y == z){
		if(color == board[x][0][0] && board[x][0][0] == board[x][1][1] && board[x][1][1] == board[x][2][2] && board[x][2][2] == board[x][3][3]){
			return 1;
		}
	}
	if(y + z == 3){
		if(color == board[x][3][0] && board[x][3][0] == board[x][2][1] && board[x][2][1] == board[x][1][2] && board[x][1][2] == board[x][0][3]){
			return 1;
		}
	}

	/* diagonals on the y-plane */
	if(x == z){
		if(color == board[0][y][0] && board[0][y][0] == board[1][y][1] && board[1][y][1] == board[2][y][2] && board[2][y][2] == board[3][y][3]){
			return 1;
		}
	}
	if(x + z == 3){
		if(color == board[3][y][0] && board[3][y][0] == board[2][y][1] && board[2][y][1] == board[1][y][2] && board[1][y][2] == board[0][y][3]){
			return 1;
		}
	}
	return 0;
}